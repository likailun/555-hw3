package edu.upenn.cis.BerkeleyStorage;

public class StorageFactory {
    public static StorageInterface getDatabaseInstance(String directory) {
//    	StorageInterface storage = new SimpleStorage();
    	StorageInterface storage = new BerkeleyInterface(directory);
    	return storage;
          
    }
}
