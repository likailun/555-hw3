package edu.upenn.cis.BerkeleyStorage;
import java.io.File;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

public class BerkelyDB {
	private static Environment env;
//	private static final String USERS = "users";
    private static final String DOCUMENT = "document";
//    private static final String CONTENT_SEEN = "content_seen";
//    private static final String CHANNEL = "channel";

//    private Database usersDb;
    private Database documentDb;
//    private Database contentseenDb;
//    private Database channelDb;

    private static final String CLASS_CATALOG = "java_class_catalog";
    private StoredClassCatalog javaCatalog;
    



public BerkelyDB(String directory) {
	EnvironmentConfig envConfig = new EnvironmentConfig();
    envConfig.setAllowCreate(true);
    envConfig.setTransactional(true);
    env = new Environment(new File(directory), envConfig);
    DatabaseConfig dbConfig = new DatabaseConfig();
    dbConfig.setTransactional(true);
    dbConfig.setAllowCreate(true);
    Database catalogDb = env.openDatabase(null, CLASS_CATALOG, 
            dbConfig);

    javaCatalog = new StoredClassCatalog(catalogDb);
//    usersDb = env.openDatabase(null, USERS, dbConfig);
    documentDb = env.openDatabase(null, DOCUMENT, dbConfig);
//    contentseenDb = env.openDatabase(null, CONTENT_SEEN, dbConfig);
//    channelDb = env.openDatabase(null, CHANNEL, dbConfig);

}


//public final Database getUserDatabase()
//{
//    return usersDb;
//}

public final Database getDocumentDatabase()
{
    return documentDb;
}

//public final Database getContentSeenDatabase()
//{
//    return contentseenDb;
//}

//public final Database getChannelDatabase()
//{
//    return channelDb;
//}

public void close() {
//	usersDb.close();
    documentDb.close();
//    contentseenDb.close();
//    channelDb.close();
    javaCatalog.close();
	env.close();		
}


public ClassCatalog getClassCatalog() {
	return javaCatalog;
}

}
