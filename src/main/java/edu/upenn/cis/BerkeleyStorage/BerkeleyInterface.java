package edu.upenn.cis.BerkeleyStorage;

import java.io.File;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

public class BerkeleyInterface implements StorageInterface{
	
	private static Environment env;
	private static final String USERS = "users";
    private static final String DOCUMENT = "document";
    private static final String CONTENT_SEEN = "content_seen";
    private Database usersDb;
    private Database documentDb;
    private Database contentseenDb;
    private static final String CLASS_CATALOG = "java_class_catalog";
    private StoredClassCatalog javaCatalog;
    
    private StoredSortedMap usersMap;
    private StoredSortedMap documentMap;
    private StoredSortedMap contentseenMap;
	private BerkelyDB db;
    private BerkeleyView views;
    
    
	public BerkeleyInterface(String directory) {

	    db = new BerkelyDB(directory);
        views = new BerkeleyView(db);
    } 
	    
	    
	
	
	
	@Override
	public int getCorpusSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int addDocument(String executorId, String key, String value) {
		Map Document = views.getDocumentMap();
		if (Document.get(executorId)==null) {
			Document.put(executorId, new DocumentValue(executorId));
		}
		DocumentValue documentValue = (DocumentValue) Document.get(executorId);
		documentValue.addKeyValue(key, value);
		Document.put(executorId, documentValue);
		System.out.println("Add key: " + key + " Value: " + value + " To DB");
//        System.out.println("BerkelyInterface: Adding Document ID is " + documentId);
        return 0;
	}

	
	@Override
	public ArrayList<String> getExecutorIdList(){
		Map Document = views.getDocumentMap();
		return new ArrayList<String>(Document.keySet());
	}

	@Override
	public void close() {
		db.close();
	}

	@Override
	public HashMap<String, ArrayList<String>> getKeyValueMapWithExecutorId(String executorId) {
		Map Document = views.getDocumentMap();
		DocumentValue documentValue =  (DocumentValue) Document.get(executorId);
		if(documentValue == null) {
			return null;
		}
		
		return documentValue.getWholeMap();
	}
	


	@Override
	public void clearDB() {
		Map Document = views.getDocumentMap();
		Document.clear();
//        System.out.println("BerkelyInterface: ContentSeen Cleared");
	}





	@Override
	public Set<String> getAllKeys(String executorId) {
		Map Document = views.getDocumentMap();
		DocumentValue documentValue =  (DocumentValue) Document.get(executorId);
		if(documentValue == null) {
			return null;
		}
		return documentValue.getAllKeys();
	}
	


}
