package edu.upenn.cis.BerkeleyStorage;
import java.math.BigInteger;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;


public class BerkeleyView {
//    private StoredSortedMap usersMap;
    private StoredSortedMap documentMap;
//    private StoredSortedMap contentseenMap;
//    private StoredSortedMap channelMap;


public BerkeleyView(BerkelyDB db) {
	ClassCatalog catalog = db.getClassCatalog();
//    EntryBinding userNameKeyBinding =
//        new SerialBinding(catalog, String.class);
//    EntryBinding userValueBinding =
//        new SerialBinding(catalog, UserValue.class);

    EntryBinding documentKeyBinding =
            new SerialBinding(catalog, String.class);
        EntryBinding documentValueBinding =
            new SerialBinding(catalog, DocumentValue.class);
        
//    EntryBinding contentSeenKeyBinding =
//            new SerialBinding(catalog, BigInteger.class);
//    EntryBinding contentSeenValueBinding =
//    		new SerialBinding(catalog, String.class);
//    
//    EntryBinding channelKeyBinding =
//            new SerialBinding(catalog, String.class);
//        EntryBinding channelValueBinding =
//            new SerialBinding(catalog, ChannelValue.class);
    
//    usersMap = new StoredSortedMap(db.getUserDatabase(),userNameKeyBinding, userValueBinding, true);
    documentMap = new StoredSortedMap(db.getDocumentDatabase(),documentKeyBinding, documentValueBinding, true);
//    contentseenMap = new StoredSortedMap(db.getContentSeenDatabase(),contentSeenKeyBinding, contentSeenValueBinding, true);
//    channelMap = new StoredSortedMap(db.getChannelDatabase(),channelKeyBinding, channelValueBinding, true);

}

//public final StoredSortedMap getUserMap()
//{
//    return usersMap;
//}

public final StoredSortedMap getDocumentMap()
{
    return documentMap;
}

//public final StoredSortedMap getcontentseenMap()
//{
//    return contentseenMap;
//}
//
//public final StoredSortedMap getchannelMap()
//{
//    return channelMap;
//}
//public final StoredEntrySet getUserEntrySet()
//{
//    return (StoredEntrySet) usersMap.entrySet();
//}

public final StoredEntrySet getDocumentEntrySet()
{
    return (StoredEntrySet) documentMap.entrySet();
}

//public final StoredEntrySet getContentSeenEntrySet()
//{
//    return (StoredEntrySet) contentseenMap.entrySet();
//}
//
//
//public final StoredEntrySet getChannelEntrySet()
//{
//    return (StoredEntrySet) channelMap.entrySet();
//}


}

