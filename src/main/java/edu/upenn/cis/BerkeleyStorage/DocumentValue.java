package edu.upenn.cis.BerkeleyStorage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class DocumentValue implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap<String, ArrayList<String>> tupleMap = new HashMap<String, ArrayList<String>>();
	String executorId;
	public DocumentValue(String executorId) {
		this.executorId = executorId;
		
		System.out.println("DB init table for " + executorId);
	}
	
	
	public void addKeyValue(String key, String value) {
		ArrayList<String> valueList = tupleMap.get(key);
		if (valueList==null) {
			tupleMap.put(key, new ArrayList<String>());
		}
		ArrayList<String> stringList = tupleMap.get(key);
		stringList.add(value);
		System.out.println("DocumentValue add " + key + " and " + value);
		System.out.println("CurrentKey is " + getAllKeys());
	}
	
	public HashMap<String, ArrayList<String>> getWholeMap(){
		return tupleMap;
	}
	
	public Set<String> getAllKeys(){
		return tupleMap.keySet();
	}
	
	public ArrayList<String> getValueList(String key){
		return tupleMap.get(key);
	}
	
	public HashMap<String, ArrayList<String>> getTupleMap() {
		return tupleMap;
	}


}
