package edu.upenn.cis.BerkeleyStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public int addDocument(String executorId, String key, String value);

    /**
     * Retrieves a document's contents by URL
     */
    public HashMap<String, ArrayList<String>> getKeyValueMapWithExecutorId(String executorId);



    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();

	public ArrayList<String> getExecutorIdList();

	public void clearDB();

	public Set<String> getAllKeys(String executorId);

}
