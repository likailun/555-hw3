package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	Fields myFields = new Fields();

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
	ConsensusTracker votesForEos;
	String outputFilePath;
	Map<String,String> config;
	@Override
	public void cleanup() {
		// Do nothing

	}
	
	public void increResult(String key, String value) {
		String result = config.get("results");
		String newResult = result + "("+key + "," + value + "),";
		config.put("results", newResult);
	}
	
    @Override
    public synchronized boolean execute(Tuple input) {
    	if (!input.isEndOfStream()) {
    		System.out.println(getExecutorId() + ": " + input.toString());
			try {
				String key = input.getStringByField("key");
		        String value = input.getStringByField("value");
		        increResult( key,  value);
		        System.out.println("Add 1 new result " + key + "values is " + value );
		        File txtFile = new File(outputFilePath);
			    if(txtFile.exists()==false){
		            txtFile.createNewFile();
		    }
				PrintWriter out = new PrintWriter(new FileWriter(txtFile, true));
				out.append("("+key + "," + value + ")\n");
				out.close();

				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
    	} else if (input.isEndOfStream()) {
    		log.debug("Processing EOS from " + input.getSourceExecutor());
	        String sourceExecutorId = input.getSourceExecutor();
    		boolean finished = votesForEos.voteForEos(sourceExecutorId);
    		
    		if (finished) {
				System.out.println("Print BolT EOS!");
				config.put("mystatus", "IDLE");
//				config.put("job", "None");
				config.put("keysRead", "0");
//				WorkerServer.shutdown();
				System.out.println("Shuting down worker");
				return true;
    		}
    	}
    	return true;
    }

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.config = stormConf;
		int executorNum = Integer.valueOf(stormConf.get("reduceExecutors"));
        int workerNum = stormConf.get("workerList").split(",").length;
        int voteNeeded = executorNum * workerNum;
        System.out.println("voteNeeded for printBolt id: " + getExecutorId() + " is " + voteNeeded);
        votesForEos = new ConsensusTracker(voteNeeded);
        outputFilePath = getOutputPath(stormConf);
        File file = new File(outputFilePath);
        file.delete();
	}
	
	public String getOutputPath(Map<String, String> stormConf) {
		String inputDirectory = (String) stormConf.get("storageDirectory");
        if(inputDirectory==null) {
        	inputDirectory = System.getProperty("user.dir") + "/storage/node" + stormConf.get("workerIndex");
        }
        else {
        	inputDirectory = System.getProperty("user.dir") + "/"+ inputDirectory;
        }
        System.out.println();
        // abs directory + storage + inpu
        String finalPath = inputDirectory + "/" + stormConf.get("outputPath");
        
        if (!Files.exists(Paths.get(finalPath))){
            try {
                Files.createDirectory(Paths.get(finalPath));
                
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
        return finalPath+ "/output.txt";
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
