package edu.upenn.cis.stormlite.bolt;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;

/**
 * A simple adapter that takes a MapReduce "Job" and calls the "map"
 * on a per-tuple basis.
 * 
 * 
 */
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class MapBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(MapBolt.class);

	Job mapJob;
	
	/**
	 * This object can help determine when we have
	 * reached enough votes for EOS
	 */
	ConsensusTracker votesForEos;

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the WordCounter, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    
	Fields schema = new Fields("key", "value");

	boolean sentEos = false;
	
	Map<String,String> config;
	/**
     * This is where we send our output stream
     */
    private OutputCollector collector;
    
    private TopologyContext context;
    
    
    public MapBolt() {
    }
    
	/**
     * Initialization, just saves the output stream destination
     */
    @Override
    public void prepare(Map<String,String> stormConf, 
    		TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.context = context;
        this.config = stormConf;
        if (!stormConf.containsKey("mapClass"))
        	throw new RuntimeException("Mapper class is not specified as a config option");
        else {
        	String mapperClass = stormConf.get("mapClass");
        	
        	try {
				mapJob = (Job)Class.forName(mapperClass).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to instantiate the class " + mapperClass);
			}
        }
        
        if (!stormConf.containsKey("spoutExecutors")) {
        	throw new RuntimeException("Mapper class doesn't know how many input spout executors");
        }
        
        // TODO: determine how many end-of-stream requests are needed, create a ConsensusTracker
        // or whatever else you need to determine when votes reach consensus
        int executorNum = Integer.valueOf(stormConf.get("spoutExecutors"));
        int workerNum = stormConf.get("workerList").split(",").length;
        int voteNeeded = executorNum * workerNum;
        System.out.println("voteNeeded for mapBolt id: " + getExecutorId() + " is " + voteNeeded);
        votesForEos = new ConsensusTracker(voteNeeded);
    }
    
    public void increKeys(String writtenORRead) {
    	 int newkeysRead = Integer.parseInt(config.get(writtenORRead)) + 1;
    	 config.put(writtenORRead, Integer.toString(newkeysRead));
    }
    
    public void clearKeys() {
    	 config.put("keysRead", "0");
    	 config.put("keysWritten", "0");
    }

    /**
     * Process a tuple received from the stream, incrementing our
     * counter and outputting a result
     */
    @Override
    public synchronized boolean execute(Tuple input) {
    	if (!input.isEndOfStream()) {
    		config.put("mystatus", "mapping");
	        String key = input.getStringByField("key");
	        String value = input.getStringByField("value");
	        String sourceExecutorId = input.getSourceExecutor();
	        increKeys("keysRead");
	       
	        log.debug(getExecutorId() + " received " + key + " / " + value + " from executor " + input.getSourceExecutor());
	        
	        if (sentEos) {
	        	throw new RuntimeException("We received data from " + input.getSourceExecutor() + " after we thought the stream had ended!");
	        }
	        
	        // TODO:  call the mapper, and do bookkeeping to track work done
	        
	        // need to implement bookkeeping inside collector
	        mapJob.map(key, value, collector, getExecutorId());
	        increKeys("keysWritten");
	        
	        
    	} else if (input.isEndOfStream()) {
    		log.debug("Processing EOS from " + input.getSourceExecutor());

    		// TODO: determine what to do with EOS messages / votes
    		
    		
    		// every executor in the first layer should send a eos to current 2nd layer executor
    		// thus the number of votes needed to # of workerserver * # of executor per workerserver at 1st layer
    		
    		// get source executor id 
    		
	        String sourceExecutorId = input.getSourceExecutor();
    		boolean finished = votesForEos.voteForEos(sourceExecutorId);
    		
    		if (finished) {
    			// if finished send # of workerserver * # of executor per workerserver at next layer times EOS
//        		int numberOfEOSToSend = 1;
//    			for (int i=0; i<numberOfEOSToSend;i++) {
//    				this.collector.emitEndOfStream(getExecutorId());
//        		}
    			// may not need to emit many times, since emitEndOfStream will emit to everything in the next layer
				this.collector.emitEndOfStream(getExecutorId());
				// emit eos and set senteso to true
				sentEos = true;
				config.put("mystatus", "waiting");
				System.out.println("Map BolT EOS!");
    		}
   
    		
    	}
    	return true;
    }

    /**
     * Shutdown, just frees memory
     */
    @Override
    public void cleanup() {
    }

    /**
     * Lets the downstream operators know our schema
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }

    /**
     * Used for debug purposes, shows our exeuctor/operator's unique ID
     */
	@Override
	public String getExecutorId() {
		return executorId;
	}

	/**
	 * Called during topology setup, sets the router to the next
	 * bolt
	 */
	@Override
	public void setRouter(StreamRouter router) {
		this.collector.setRouter(router);
	}

	/**
	 * The fields (schema) of our output stream
	 */
	@Override
	public Fields getSchema() {
		return schema;
	}
}
