package edu.upenn.cis.stormlite.spout;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class FileSpoutHandler extends FileSpout {
	
	@Override
	public String getFilename(Map conf) {

        String inputDirectory = (String) conf.get("storageDirectory");
        if(inputDirectory==null) {
        	inputDirectory = System.getProperty("user.dir") + "/storage/node" + conf.get("workerIndex");
        }
        else {
        	inputDirectory = System.getProperty("user.dir") + "/"+ inputDirectory;
        }
        System.out.println();
        // abs directory + storage + inpu
        String finalPath = inputDirectory + "/" + conf.get("inputPath");
        
        if (!Files.exists(Paths.get(inputDirectory))){
            try {
                Files.createDirectory(Paths.get(inputDirectory));
                
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		File folder = new File(finalPath);
		File[] listOfFiles = folder.listFiles();
		String[] filename = listOfFiles[0].getName().split("\\.");
		String finalName = filename[0] + "." + filename[1];
		return finalPath +"/" +finalName;
	}

}
