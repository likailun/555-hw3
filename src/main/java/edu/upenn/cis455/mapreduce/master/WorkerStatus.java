package edu.upenn.cis455.mapreduce.master;

public class WorkerStatus {
	String port;
	String status;
	String job;
	String keysRead;
	String keysWritten;
	String result;
	String index;
	long lastSavedTime;
	public WorkerStatus(String port, String status, String job, String keysRead, String keysWritten, String result, String index) {
		this.port = port;
		this.status = status;
		this.job = job;
		this.keysRead = keysRead;
		this.keysWritten = keysWritten;
		this.result = result;
		long lastSavedTime = System.currentTimeMillis();
		this.index = index;
	}
	
	public String getPort() {
		return port;
	}
	
	public String getstatus() {
		return status;
	}
	public String getjob() {
		if(job==null) {
			return "None";
		}
		return job;
	}
	public String getKeysRead() {
		return keysRead;
	}
	public String getkeysWritten() {
		return keysWritten;
	}
	public String getResult() {
		String parsedResult = result;
			if (parsedResult.contains(",")) {
				parsedResult = result.substring(0, result.length() - 1);
			}
			return"["  + parsedResult + "]";
		
	}
	
	public void setStatus(String newStatus) {
		status = newStatus;
	}
	
	public void setjob(String newjob) {
		job = newjob;
	}
	
	public void setkeysRead(String newRead) {
		keysRead = newRead;
	}
	
	public void setkeysWritten(String newWritten) {
		keysWritten = newWritten;
	}
	
	public void setResult(String newResult) {
		result = newResult;
	}
	
	public void setPort(String newport) {
		port = newport;
	}
	public void updateTime() {
		lastSavedTime = System.currentTimeMillis();
	}
	
	public boolean isActive() {
		if(System.currentTimeMillis()-lastSavedTime > 30000) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public String getIndex() {
		return index;
	}
}
