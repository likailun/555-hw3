package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import edu.upenn.cis.stormlite.Config;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.PrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.FileSpoutHandler;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.master.MasterServer;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
// status 

public class MasterServer { 
	public static String htmlForm = 
			"<form method=\"POST\" action=\"/submitjob\">\r\n"
			+ "Job Name: <input type=\"text\" name=\"jobname\"/><br/>\r\n"
			+ "Class Name: <input type=\"text\" name=\"classname\"/><br/>\r\n "
			+ "Input Directory: <input type=\"text\" name=\"input\"/><br/>\r\n "
			+ "Output Directory: <input type=\"text\" name=\"output\"/><br/>\r\n "
			+ "Map Threads: <input type=\"text\" name=\"map\"/><br/>\r\n"
			+ "Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\r\n"
			+ "<input type=\"submit\" value=\"Submit\">\r\n"
			+ "</form>\n";
	static Logger log = LogManager.getLogger(MasterServer.class);

	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
    public static HashMap<String,WorkerStatus> workerStatusMap;
    public static int indexCount = 0;
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");
            String statusHTML = parseStatus();
            return ("<html><head><title>Status</title></head>\n" + 
                    "<body>\n" + statusHTML +htmlForm + "</body></html>");
        });

    }
    
    public static String parseStatus() {
    	String finalHtml = "";
    	Set<String> workerKeysSet = workerStatusMap.keySet();
    	int workerNum = 0;
    	for (String workerKey: workerKeysSet) {
        	WorkerStatus workerstatus = workerStatusMap.get(workerKey);
        	if(workerstatus.isActive()) {
        		finalHtml += "<div>" +  workerNum + ": port=" + workerstatus.getPort()+", status="+workerstatus.getstatus()
        		+", job="+workerstatus.getjob()+", keysRead=" + workerstatus.getKeysRead() 
        		+ ", keysWritten="+workerstatus.getkeysWritten()+", results="+workerstatus.getResult() + "\n";
        		workerNum++;
        	}
    	}
    	return finalHtml;
    	
    }
    
    public static void submitForm(Config config ) {
        post("/submitjob", (request,response)->{
        	String jobname = request.queryParams("jobname");
        	String classname = request.queryParams("classname");
        	String inputDirectory = request.queryParams("input");
        	String outputDirectory = request.queryParams("output");
        	String numberMapExecutor = request.queryParams("map");
        	String numberReduceExecutor = request.queryParams("reduce");
            config.put("inputPath", inputDirectory);
            config.put("outputPath", outputDirectory);
            config.put("job", jobname);
            config.put("mapClass", classname);
            config.put("reduceClass", classname);
            config.put("spoutExecutors", "1");
            config.put("mapExecutors", numberMapExecutor);
            config.put("reduceExecutors", numberReduceExecutor);
            
            //start the whole thing, if recived the form
            buildTopology(config);
            
        	return "Submitted";
            
        });
    }
    
    public static void workerStatus(Config config) {
        get("/workerstatus", (request,response)->{
        	String port = request.queryParams("port");
        	String job = request.queryParams("job");
        	String status = request.queryParams("status");
        	String keysRead = request.queryParams("keysRead");
        	String keysWritten = request.queryParams("keysWritten");
        	String results = request.queryParams("results");
        	String workerIp = request.ip();
        	
        	String workerKey = workerIp + ":" + port;
        	System.out.println("Recieved status from worker " + workerKey);
        	if (!workerStatusMap.containsKey(workerKey)) {
        		WorkerStatus workerstatus = new WorkerStatus( port,  status,  job,  keysRead,  keysWritten, results, Integer.toString(indexCount));
        		workerStatusMap.put(workerKey, workerstatus);
        		indexCount++;
        	}
        	WorkerStatus workerstatus = workerStatusMap.get(workerKey);
        	workerstatus.setjob(job);
        	workerstatus.setkeysRead(keysRead);
        	workerstatus.setkeysWritten(keysWritten);
        	workerstatus.setResult(results);
        	workerstatus.setStatus(status);
        	workerstatus.setPort(port);
        	workerstatus.updateTime();
        	workerStatusMap.put(workerKey, workerstatus);
        	return "Recived Worker Status";
            
        });
    }
    
    
    
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		log.info("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();
		
		return conn;
    }
    
    public static String getWorkerList() {
    	String workerlist = "[";
    	Set<String> workerKeysSet = workerStatusMap.keySet();
    	for (String workerKey: workerKeysSet) {
    		workerlist = workerlist + workerKey + ",";
    	}
    	workerlist = workerlist.substring(0, workerlist.length() - 1);
    	workerlist += "]";
    	System.out.println("workerlist is " + workerlist);
    	return workerlist;
    	
    }
    
    public static void buildTopology(Config config) throws IOException {
    	
    	config.put("workerList", getWorkerList());
        FileSpout spout = new FileSpoutHandler();
        MapBolt bolt = new MapBolt();
        ReduceBolt bolt2 = new ReduceBolt();
        PrintBolt printer = new PrintBolt();

        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));
        
        // Parallel mappers, each of which gets specific words
        builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
        
        // Parallel reducers, each of which gets specific words
        builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

        // Only use the first printer bolt for reducing to a single point
        builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);
        
        Topology topo = builder.createTopology();
        
        WorkerJob job = new WorkerJob(topo, config);
		
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		try {
			String[] workers = WorkerHelper.getWorkers(config);

			int i = 0;
			for (String dest: workers) {
		        config.put("workerIndex", String.valueOf(i++));
				if (sendJob(dest, "POST", config, "definejob", 
						mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job definition request failed");
				}
			}
			for (String dest: workers) {
				if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job execution request failed");
				}
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	        System.exit(0);
		}   
    }
    
    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }
        Config config = new Config();
        int myPort = Integer.valueOf(args[0]);
        port(myPort);
        workerStatusMap = new HashMap<String,WorkerStatus>();
        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here
        workerStatus(config);
        registerStatusPage();
        submitForm(config);   // listen to form submission
        get("/shutdown", (request, response) -> {
        	for (String dest: WorkerHelper.getWorkers(config)) {    			
    			HttpURLConnection con = (HttpURLConnection) new URL(dest+"/shutdown").openConnection();
    			con.setRequestMethod("GET");
    			int code = con.getResponseCode();}
        	stop();
        	return "shutting down workers";});
        // TODO: route handler for /workerstatus reports from the workers
    
    }
}

