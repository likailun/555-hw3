package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static Logger log = LogManager.getLogger(WorkerServer.class);

    static DistributedCluster cluster = new DistributedCluster();

    List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();

    static boolean stillworking = true;
    
    public WorkerServer(int myPort, String masterAddress, String storagePath) throws MalformedURLException {

        log.info("Creating server listener at socket " + myPort);
        port(myPort);
        //send current status to server
        
        
        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Spark.post("/definejob", (req, res) -> {

            WorkerJob workerJob;
            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);
                try {
                	Map<String, String> config = workerJob.getConfig();
                	initStatus(config, String.valueOf(myPort), config.get("job"), "IDLE");
                	config.put("storageDirectory", storagePath);
                	backgroundThread(config, masterAddress);
                    log.info("Processing job definition request" + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(), 
                            workerJob.getTopology()));

                    // Add a new topology
                    synchronized (topologies) {
                        topologies.add(workerJob.getConfig().get("job"));
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();

                // Internal server error
                res.status(500);
                return e.getMessage();
            } 

        });

        Spark.post("/runjob", new RunJobRoute(cluster));
        
        Spark.get("/shutdown", (req, res) -> {
        	shutdown();
        	return "shuting down worker";
        });
        
        Spark.post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
//                System.out.println("Worker received: " + req.body());
                log.debug("Worker received: " + req.body());
                Tuple tuple = om.readValue(req.body(), Tuple.class);

                log.debug("Worker received: " + tuple + " for " + stream);

                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);

                if (contexts.isEmpty())
                    log.error("No topology context -- were we initialized??");

                TopologyContext ourContext = contexts.get(contexts.size() - 1);

                // Instrumentation for tracking progress
                if (!tuple.isEndOfStream())
                    ourContext.incSendOutputs(router.getKey(tuple.getValues()));

                // TODO: handle tuple vs end of stream for our *local nodes only*
                // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                if (tuple.isEndOfStream())
                	router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
                else
                    router.executeLocally(tuple, ourContext, tuple.getSourceExecutor());
                return "OK";
                
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                res.status(500);
                return e.getMessage();
            }

        });

    }

    public static void createWorker(Map<String, String> config) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker spout doesn't know its worker ID");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

//                new WorkerServer(url.getPort());
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }
        stillworking = false;
        cluster.shutdown();
        stop();
    }
    
    public void backgroundThread(Map<String, String> config, String masterAddress) {
    	Runnable r = new Runnable() {
            public void run() {
            	while(stillworking) {
            	try {
            	String port = config.get("myPort");
            	String status = config.get("mystatus");
            	String job = config.get("job");
            	String keysRead = config.get("keysRead");
            	String keysWritten = config.get("keysWritten");
            	String results = config.get("results");
            	String finalUrl = "http://" + masterAddress + "/workerstatus?" + "port=" +port
            			+ "&status="  + status + "&job=" + job + "&keysRead=" + keysRead
            			+ "&keysWritten=" + keysWritten + "&results=" + results;
                URL url = new URL(finalUrl);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				System.out.println("Send status to master with url "  + finalUrl 
						+ " from port " + port + "Status Code is" + con.getResponseCode());
        		Thread.sleep(10000);
				} catch (IOException | InterruptedException e) {
					continue;
				}
            	}
            }
        };

        new Thread(r).start();
    }
    
    public void initStatus(Map<String, String> config, String myPort, String job, String mystatus) {
    	config.put("myPort", myPort);
    	config.put("mystatus", mystatus);
    	config.put("job", job);
    	config.put("keysRead", "0");
    	config.put("keysWritten", "0");
    	config.put("results", "");
    }
    
    

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws MalformedURLException {
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);

        System.out.println("Worker node startup, on port " + myPort);
        
        String masterAddress = args[1];
        String storagePath = args[2];
        
        //send init ip address
    	String port = String.valueOf(myPort);
    	String status = "IDLE";
    	String job = "None";
    	String keysRead = "0";
    	String keysWritten = "0";
    	String results = "";
    	String finalUrl = "http://"+masterAddress + "/workerstatus?" + "port=" +port
    			+ "&status="  + status + "&job=" + job + "&keysRead=" + keysRead
    			+ "&keysWritten=" + keysWritten + "&results=" + results;
        URL url = new URL(finalUrl);
        HttpURLConnection con;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			System.out.println(con.getResponseCode());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        WorkerServer worker = new WorkerServer(myPort, masterAddress, storagePath);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce
        // here
    }
}
