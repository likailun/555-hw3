package edu.upenn.cis.stormlite.mapreduce;

import java.util.Map;

import edu.upenn.cis.stormlite.spout.FileSpout;

public class WordFileSpout extends FileSpout {

	@Override
	public String getFilename(Map conf) {
		return "words.txt";
	}

}
