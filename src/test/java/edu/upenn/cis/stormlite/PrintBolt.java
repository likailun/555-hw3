package edu.upenn.cis.stormlite;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	Fields myFields = new Fields();

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
	ConsensusTracker votesForEos;

	@Override
	public void cleanup() {
		// Do nothing

	}
	
    @Override
    public synchronized boolean execute(Tuple input) {
    	if (!input.isEndOfStream()) {
    		System.out.println(getExecutorId() + ": " + input.toString());
	    
    	} else if (input.isEndOfStream()) {
    		log.debug("Processing EOS from " + input.getSourceExecutor());
	        String sourceExecutorId = input.getSourceExecutor();
    		boolean finished = votesForEos.voteForEos(sourceExecutorId);
    		
    		if (finished) {
				System.out.println("Print BolT EOS!");
				return true;
    		}
    	}
    	return true;
    }

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		int executorNum = Integer.valueOf(stormConf.get("reduceExecutors"));
        int workerNum = stormConf.get("workerList").split(",").length;
        int voteNeeded = executorNum * workerNum;
        System.out.println("voteNeeded for printBolt id: " + getExecutorId() + " is " + voteNeeded);
        votesForEos = new ConsensusTracker(voteNeeded);
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
